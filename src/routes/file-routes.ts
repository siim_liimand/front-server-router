import { IsRoute } from '@dixid/front-router/dist/interfaces';
import { AddFileRoute, GetFileRoute, FileRoute, FileRoutes, GetFileResponse } from '../interfaces';
import { TYPE_FILE } from './constants';

const PREFIX = '/files/';
const exceptions = ['/robots.txt', '/sitemaps.xml'];
const fileRoutes: FileRoutes = [];

const isFileRoute: IsRoute = (path) => {
  return path.startsWith(PREFIX) || exceptions.includes(path);
};

export const addFileRoute: AddFileRoute = (path) => {
  const realPath = exceptions.includes(path) ? path : PREFIX + path;
  const route: FileRoute = [
    realPath,
    (getFileResponse: GetFileResponse) => {
      getFileResponse(realPath.substring(1));
    },
  ];
  fileRoutes.push(route);
};

const getFileRoute: GetFileRoute = (path) => {
  const finder = ([p]: FileRoute) => p === path;
  const route = fileRoutes.find(finder);
  if (route) {
    route[2] = TYPE_FILE;
    return route;
  }
};

export { isFileRoute, getFileRoute };
