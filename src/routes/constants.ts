import {
  ROUTE_TYPE_API,
  ROUTE_TYPE_FILE,
  ROUTE_TYPE_IMAGE,
  ROUTE_TYPE_JS,
  ROUTE_TYPE_PAGE,
} from '@dixid/front-router/dist/interfaces';

export const TYPE_PAGE: ROUTE_TYPE_PAGE = 'page';
export const TYPE_FILE: ROUTE_TYPE_FILE = 'file';
export const TYPE_IMAGE: ROUTE_TYPE_IMAGE = 'image';
export const TYPE_JS: ROUTE_TYPE_JS = 'js';
export const TYPE_API: ROUTE_TYPE_API = 'api';
