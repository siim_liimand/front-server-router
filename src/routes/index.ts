import { AllGetRoutes } from '../interfaces';
import { getApiRoute, isApiRoute } from './api-routes';
import { getFileRoute, isFileRoute } from './file-routes';
import { getImageRoute, isImageRoute } from './image-routes';
import { getJsRoute, isJsRoute } from './js-routes';
import { getPageRoute } from './page-routes';

export const getRoute: AllGetRoutes = (path, getFormattedRoutePath) => {
  if (isApiRoute(path)) {
    return getApiRoute(path);
  }

  if (isJsRoute(path)) {
    return getJsRoute(path);
  }

  if (isImageRoute(path)) {
    return getImageRoute(path);
  }

  if (isFileRoute(path)) {
    return getFileRoute(path);
  }

  const pagePath = getFormattedRoutePath ? getFormattedRoutePath() : path;

  return getPageRoute(pagePath);
};
