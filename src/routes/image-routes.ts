import { IsRoute } from '@dixid/front-router/dist/interfaces';
import { AddImageRoute, GetImageResponse, GetImageRoute, ImageRoute, ImageRoutes } from '../interfaces';
import { TYPE_IMAGE } from './constants';

const PREFIX = '/images/';
const exceptions = ['/favicon.ico'];
const imageRoutes: ImageRoutes = [];

const isImageRoute: IsRoute = (path) => {
  return path.startsWith('/images/') || exceptions.includes(path);
};

export const addImageRoute: AddImageRoute = (path) => {
  const realPath = exceptions.includes(path) ? path : PREFIX + path;
  const route: ImageRoute = [
    realPath,
    (getImageResponse: GetImageResponse) => {
      getImageResponse(realPath.substring(1));
    },
  ];
  imageRoutes.push(route);
};

const getImageRoute: GetImageRoute = (path) => {
  const finder = ([p]: ImageRoute) => p === path;
  const route = imageRoutes.find(finder);
  if (route) {
    route[2] = TYPE_IMAGE;
    return route;
  }
};

export { isImageRoute, getImageRoute };
