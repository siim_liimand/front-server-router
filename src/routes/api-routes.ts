import { IsRoute } from '@dixid/front-router/dist/interfaces';
import { AddApiRoute, GetApiRoute, ApiRoute, ApiRoutes } from '../interfaces';
import { TYPE_API } from './constants';

const PREFIX = '/api/';
const apiRoutes: ApiRoutes = [];

const isApiRoute: IsRoute = (path) => {
  return path.startsWith(PREFIX);
};

export const addApiRoute: AddApiRoute = (path, getApiResponseCallback) => {
  if (getApiResponseCallback) {
    const realPath = PREFIX + path;
    const route: ApiRoute = [realPath, getApiResponseCallback];
    apiRoutes.push(route);
  }
};

const getApiRoute: GetApiRoute = (path) => {
  const finder = ([p]: ApiRoute) => p === path;
  const route = apiRoutes.find(finder);
  if (route) {
    route[2] = TYPE_API;
    return route;
  }
};

export { isApiRoute, getApiRoute };
