import { IsRoute } from '@dixid/front-router/dist/interfaces';
import { AddJsRoute, GetJsContent, GetJsRoute, JsRoute, JsRoutes } from '../interfaces';
import { TYPE_JS } from './constants';

const PREFIX = '/js';
const exceptions: string[] = [];
const jsRoutes: JsRoutes = [];

const isJsRoute: IsRoute = (path) => {
  return path.endsWith('.js');
};

export const addJsRoute: AddJsRoute = (path) => {
  const realPath = exceptions.includes(path) ? path : PREFIX + path;
  const route: JsRoute = [
    realPath,
    (getJsContent: GetJsContent) => {
      getJsContent(realPath.substring(1));
    },
  ];
  jsRoutes.push(route);
};

const getJsRoute: GetJsRoute = (path) => {
  const finder = ([p]: JsRoute) => p === PREFIX + path;
  const route = jsRoutes.find(finder);
  if (route) {
    route[2] = TYPE_JS;
    return route;
  }
};

export { isJsRoute, getJsRoute };
