import { AddPageRoute, PageRoutes, PageRoute, GetPageRoute } from '@dixid/front-router/dist/interfaces';
import { TYPE_PAGE } from './constants';

const pageRoutes: PageRoutes = [];

export const addPageRoute: AddPageRoute = (routeKey, getPageComponentCallaback): void => {
  if (getPageComponentCallaback) {
    const route: PageRoute = [routeKey, getPageComponentCallaback];
    pageRoutes.push(route);
  }
};

export const getPageRoute: GetPageRoute = (path) => {
  const finder = ([key]: PageRoute) => key === path;
  const route: PageRoute | undefined = pageRoutes.find(finder);
  if (route) {
    route[2] = TYPE_PAGE;
    return route;
  }
};
