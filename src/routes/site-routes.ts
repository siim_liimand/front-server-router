import { AddSiteRoute, GetSiteRoute, SiteRoute, SiteRoutes } from '../interfaces';

const siteRoutes: SiteRoutes = [];

export const addSiteRoute: AddSiteRoute = (routeKey, getSiteDataCallback): void => {
  const route: SiteRoute = [routeKey, getSiteDataCallback];
  siteRoutes.push(route);
};

export const getSiteRoute: GetSiteRoute = (routeKey) => {
  const finder = ([key]: SiteRoute) => key === routeKey;
  return siteRoutes.find(finder);
};
