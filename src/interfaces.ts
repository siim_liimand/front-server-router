import {
  AddRoute,
  GetFormattedRoutePath,
  GetRoute,
  GetRouteContent,
  GetRouteContentCallback,
  PageComponent,
  PageOptions,
  PageRoute,
  Route,
} from '@dixid/front-router/dist/interfaces';

export type SiteRoute = [hostName: string, siteName: string];
export type SiteRoutes = SiteRoute[];
export type AddSiteRoute = (hostName: string, siteName: string) => void;
export type GetSiteRoute = (hostName: string) => SiteRoute | undefined;

export type ApiResponse = Record<string, unknown>;
export interface ApiOptions {
  method: 'GET' | 'POST' | 'PUT' | 'DELETE' | 'OPTIONS';
  query: string;
  data?: Record<string, unknown>;
}
export type GetApiResponse = GetRouteContent<ApiResponse>;
export type GetApiResponseCallback = GetRouteContentCallback<ApiResponse, ApiOptions>;
export type ApiRoute = Route<ApiResponse, ApiOptions>;
export type ApiRoutes = ApiRoute[];
export type AddApiRoute = AddRoute<ApiResponse, ApiOptions>;
export type GetApiRoute = GetRoute<ApiResponse, ApiOptions>;

export type ImageContent = string;
export type ImageOptions = unknown;
export type GetImageResponse = GetRouteContent<ImageContent>;
export type GetImageResponseCallback = GetRouteContentCallback<ImageContent, ImageOptions>;
export type ImageRoute = Route<ImageContent, ImageOptions>;
export type ImageRoutes = ImageRoute[];
export type AddImageRoute = AddRoute<ImageContent, ImageOptions>;
export type GetImageRoute = GetRoute<ImageContent, ImageOptions>;

export type FileContent = string;
export type FileOptions = unknown;
export type GetFileResponse = GetRouteContent<FileContent>;
export type GetFileResponseCallback = GetRouteContentCallback<FileContent, FileOptions>;
export type FileRoute = Route<FileContent, FileOptions>;
export type FileRoutes = FileRoute[];
export type AddFileRoute = AddRoute<FileContent, FileOptions>;
export type GetFileRoute = GetRoute<FileContent, FileOptions>;

export type JsContent = string;
export type JsOptions = unknown;
export type GetJsContent = GetRouteContent<JsContent>;
export type GetJsContentCallback = GetRouteContentCallback<JsContent, JsOptions>;
export type AddJsRoute = AddRoute<JsContent, JsOptions>;
export type JsRoute = Route<JsContent, JsOptions>;
export type JsRoutes = JsRoute[];
export type GetJsRoute = GetRoute<JsContent, JsOptions>;

export type AllRoutes = PageComponent | JsContent | FileContent | ImageContent | ApiResponse;
export type AllOptions = PageOptions | JsOptions | FileOptions | ImageOptions | ApiOptions;
export type AllGetRoutes = (
  path: string,
  getFormattedRoutePath?: GetFormattedRoutePath,
) => PageRoute | JsRoute | FileRoute | ImageRoute | ApiRoute | undefined;
