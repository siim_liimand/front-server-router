"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getApiRoute = exports.isApiRoute = exports.addApiRoute = void 0;
const constants_1 = require("./constants");
const PREFIX = '/api/';
const apiRoutes = [];
const isApiRoute = (path) => {
    return path.startsWith(PREFIX);
};
exports.isApiRoute = isApiRoute;
const addApiRoute = (path, getApiResponseCallback) => {
    if (getApiResponseCallback) {
        const realPath = PREFIX + path;
        const route = [realPath, getApiResponseCallback];
        apiRoutes.push(route);
    }
};
exports.addApiRoute = addApiRoute;
const getApiRoute = (path) => {
    const finder = ([p]) => p === path;
    const route = apiRoutes.find(finder);
    if (route) {
        route[2] = constants_1.TYPE_API;
        return route;
    }
};
exports.getApiRoute = getApiRoute;
//# sourceMappingURL=api-routes.js.map