"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getImageRoute = exports.isImageRoute = exports.addImageRoute = void 0;
const constants_1 = require("./constants");
const PREFIX = '/images/';
const exceptions = ['/favicon.ico'];
const imageRoutes = [];
const isImageRoute = (path) => {
    return path.startsWith('/images/') || exceptions.includes(path);
};
exports.isImageRoute = isImageRoute;
const addImageRoute = (path) => {
    const realPath = exceptions.includes(path) ? path : PREFIX + path;
    const route = [
        realPath,
        (getImageResponse) => {
            getImageResponse(realPath.substring(1));
        },
    ];
    imageRoutes.push(route);
};
exports.addImageRoute = addImageRoute;
const getImageRoute = (path) => {
    const finder = ([p]) => p === path;
    const route = imageRoutes.find(finder);
    if (route) {
        route[2] = constants_1.TYPE_IMAGE;
        return route;
    }
};
exports.getImageRoute = getImageRoute;
//# sourceMappingURL=image-routes.js.map