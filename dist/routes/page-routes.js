"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getPageRoute = exports.addPageRoute = void 0;
const constants_1 = require("./constants");
const pageRoutes = [];
const addPageRoute = (routeKey, getPageComponentCallaback) => {
    if (getPageComponentCallaback) {
        const route = [routeKey, getPageComponentCallaback];
        pageRoutes.push(route);
    }
};
exports.addPageRoute = addPageRoute;
const getPageRoute = (path) => {
    const finder = ([key]) => key === path;
    const route = pageRoutes.find(finder);
    if (route) {
        route[2] = constants_1.TYPE_PAGE;
        return route;
    }
};
exports.getPageRoute = getPageRoute;
//# sourceMappingURL=page-routes.js.map