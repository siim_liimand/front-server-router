"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getJsRoute = exports.isJsRoute = exports.addJsRoute = void 0;
const constants_1 = require("./constants");
const PREFIX = '/js';
const exceptions = [];
const jsRoutes = [];
const isJsRoute = (path) => {
    return path.endsWith('.js');
};
exports.isJsRoute = isJsRoute;
const addJsRoute = (path) => {
    const realPath = exceptions.includes(path) ? path : PREFIX + path;
    const route = [
        realPath,
        (getJsContent) => {
            getJsContent(realPath.substring(1));
        },
    ];
    jsRoutes.push(route);
};
exports.addJsRoute = addJsRoute;
const getJsRoute = (path) => {
    const finder = ([p]) => p === PREFIX + path;
    const route = jsRoutes.find(finder);
    if (route) {
        route[2] = constants_1.TYPE_JS;
        return route;
    }
};
exports.getJsRoute = getJsRoute;
//# sourceMappingURL=js-routes.js.map