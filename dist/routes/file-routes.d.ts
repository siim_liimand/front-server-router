import { IsRoute } from '@dixid/front-router/dist/interfaces';
import { AddFileRoute, GetFileRoute } from '../interfaces';
declare const isFileRoute: IsRoute;
export declare const addFileRoute: AddFileRoute;
declare const getFileRoute: GetFileRoute;
export { isFileRoute, getFileRoute };
//# sourceMappingURL=file-routes.d.ts.map