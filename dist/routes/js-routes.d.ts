import { IsRoute } from '@dixid/front-router/dist/interfaces';
import { AddJsRoute, GetJsRoute } from '../interfaces';
declare const isJsRoute: IsRoute;
export declare const addJsRoute: AddJsRoute;
declare const getJsRoute: GetJsRoute;
export { isJsRoute, getJsRoute };
//# sourceMappingURL=js-routes.d.ts.map