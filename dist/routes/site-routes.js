"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getSiteRoute = exports.addSiteRoute = void 0;
const siteRoutes = [];
const addSiteRoute = (routeKey, getSiteDataCallback) => {
    const route = [routeKey, getSiteDataCallback];
    siteRoutes.push(route);
};
exports.addSiteRoute = addSiteRoute;
const getSiteRoute = (routeKey) => {
    const finder = ([key]) => key === routeKey;
    return siteRoutes.find(finder);
};
exports.getSiteRoute = getSiteRoute;
//# sourceMappingURL=site-routes.js.map