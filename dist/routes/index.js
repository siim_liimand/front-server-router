"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getRoute = void 0;
const api_routes_1 = require("./api-routes");
const file_routes_1 = require("./file-routes");
const image_routes_1 = require("./image-routes");
const js_routes_1 = require("./js-routes");
const page_routes_1 = require("./page-routes");
const getRoute = (path, getFormattedRoutePath) => {
    if ((0, api_routes_1.isApiRoute)(path)) {
        return (0, api_routes_1.getApiRoute)(path);
    }
    if ((0, js_routes_1.isJsRoute)(path)) {
        return (0, js_routes_1.getJsRoute)(path);
    }
    if ((0, image_routes_1.isImageRoute)(path)) {
        return (0, image_routes_1.getImageRoute)(path);
    }
    if ((0, file_routes_1.isFileRoute)(path)) {
        return (0, file_routes_1.getFileRoute)(path);
    }
    const pagePath = getFormattedRoutePath ? getFormattedRoutePath() : path;
    return (0, page_routes_1.getPageRoute)(pagePath);
};
exports.getRoute = getRoute;
//# sourceMappingURL=index.js.map