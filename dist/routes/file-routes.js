"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getFileRoute = exports.isFileRoute = exports.addFileRoute = void 0;
const constants_1 = require("./constants");
const PREFIX = '/files/';
const exceptions = ['/robots.txt', '/sitemaps.xml'];
const fileRoutes = [];
const isFileRoute = (path) => {
    return path.startsWith(PREFIX) || exceptions.includes(path);
};
exports.isFileRoute = isFileRoute;
const addFileRoute = (path) => {
    const realPath = exceptions.includes(path) ? path : PREFIX + path;
    const route = [
        realPath,
        (getFileResponse) => {
            getFileResponse(realPath.substring(1));
        },
    ];
    fileRoutes.push(route);
};
exports.addFileRoute = addFileRoute;
const getFileRoute = (path) => {
    const finder = ([p]) => p === path;
    const route = fileRoutes.find(finder);
    if (route) {
        route[2] = constants_1.TYPE_FILE;
        return route;
    }
};
exports.getFileRoute = getFileRoute;
//# sourceMappingURL=file-routes.js.map