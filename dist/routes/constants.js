"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TYPE_API = exports.TYPE_JS = exports.TYPE_IMAGE = exports.TYPE_FILE = exports.TYPE_PAGE = void 0;
exports.TYPE_PAGE = 'page';
exports.TYPE_FILE = 'file';
exports.TYPE_IMAGE = 'image';
exports.TYPE_JS = 'js';
exports.TYPE_API = 'api';
//# sourceMappingURL=constants.js.map