import { IsRoute } from '@dixid/front-router/dist/interfaces';
import { AddImageRoute, GetImageRoute } from '../interfaces';
declare const isImageRoute: IsRoute;
export declare const addImageRoute: AddImageRoute;
declare const getImageRoute: GetImageRoute;
export { isImageRoute, getImageRoute };
//# sourceMappingURL=image-routes.d.ts.map