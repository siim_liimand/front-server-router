import { IsRoute } from '@dixid/front-router/dist/interfaces';
import { AddApiRoute, GetApiRoute } from '../interfaces';
declare const isApiRoute: IsRoute;
export declare const addApiRoute: AddApiRoute;
declare const getApiRoute: GetApiRoute;
export { isApiRoute, getApiRoute };
//# sourceMappingURL=api-routes.d.ts.map