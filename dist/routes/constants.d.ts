import { ROUTE_TYPE_API, ROUTE_TYPE_FILE, ROUTE_TYPE_IMAGE, ROUTE_TYPE_JS, ROUTE_TYPE_PAGE } from '@dixid/front-router/dist/interfaces';
export declare const TYPE_PAGE: ROUTE_TYPE_PAGE;
export declare const TYPE_FILE: ROUTE_TYPE_FILE;
export declare const TYPE_IMAGE: ROUTE_TYPE_IMAGE;
export declare const TYPE_JS: ROUTE_TYPE_JS;
export declare const TYPE_API: ROUTE_TYPE_API;
//# sourceMappingURL=constants.d.ts.map