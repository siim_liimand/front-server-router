"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getRoute = exports.getSiteRoute = exports.addSiteRoute = exports.addPageRoute = exports.addJsRoute = exports.addImageRoute = exports.addFileRoute = exports.addApiRoute = void 0;
var api_routes_1 = require("./routes/api-routes");
Object.defineProperty(exports, "addApiRoute", { enumerable: true, get: function () { return api_routes_1.addApiRoute; } });
var file_routes_1 = require("./routes/file-routes");
Object.defineProperty(exports, "addFileRoute", { enumerable: true, get: function () { return file_routes_1.addFileRoute; } });
var image_routes_1 = require("./routes/image-routes");
Object.defineProperty(exports, "addImageRoute", { enumerable: true, get: function () { return image_routes_1.addImageRoute; } });
var js_routes_1 = require("./routes/js-routes");
Object.defineProperty(exports, "addJsRoute", { enumerable: true, get: function () { return js_routes_1.addJsRoute; } });
var page_routes_1 = require("./routes/page-routes");
Object.defineProperty(exports, "addPageRoute", { enumerable: true, get: function () { return page_routes_1.addPageRoute; } });
var site_routes_1 = require("./routes/site-routes");
Object.defineProperty(exports, "addSiteRoute", { enumerable: true, get: function () { return site_routes_1.addSiteRoute; } });
Object.defineProperty(exports, "getSiteRoute", { enumerable: true, get: function () { return site_routes_1.getSiteRoute; } });
var index_1 = require("./routes/index");
Object.defineProperty(exports, "getRoute", { enumerable: true, get: function () { return index_1.getRoute; } });
//# sourceMappingURL=index.js.map